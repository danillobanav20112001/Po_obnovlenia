#define _CRT_SECURE_NO_WARNINGS
#define CREATE_NO_WINDOW
//#define UNICODE
//#define _UNICODE
// IRIupdater.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//                                                   || IRI - Updater - Программа для обновления ArmBase ||
#include <windows.h>
#include <iostream>
#include <fstream>
#include <boost/filesystem.hpp>
#include <boost\thread.hpp>
#include <boost\range\iterator_range.hpp>
#include <boost/program_options.hpp>
#include <boost/interprocess/sync/file_lock.hpp>
#include <boost/interprocess/sync/sharable_lock.hpp>
#include <filesystem>
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/attributes/current_process_name.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <string>
#include <tlhelp32.h>
#include <time.h>
#include "WinError.h"
//#include "resource.h" - Подключить при создании окна прогресса
namespace fs = boost::filesystem;
using namespace std;
namespace po = boost::program_options;

const char* NO_SIGNAL = ""; ///<Плейс холдер для того что бы определить что нет значения

/**
 * Класс для результатов проверки
 */
class SCheckResult
{
    int chekFile = 0;
    int checkExe = 0;

public:
    SCheckResult& operator+=(SCheckResult& other) {
        chekFile += other.chekFile;
        checkExe += other.checkExe;
        return *this;
    }

    SCheckResult& calculate_file(const fs::directory_entry& item) {
        if (fs::extension(item) == ".exe") {
            checkExe++;
        } else {
            chekFile++;
        }
        return *this;
    }

    bool is_have_update() const {
        return chekFile == 0 && checkExe == 0;
    }

    bool is_only_exe_in_update() const {
        return chekFile == 0 && checkExe > 0;
    }
};

/**
 * Находить ID процесса по имени. Если он есть в системе
 *
 * \param processName
 * \return
 */
DWORD FindProcessId(const std::wstring& processName)
{
    PROCESSENTRY32 processInfo;
    processInfo.dwSize = sizeof(processInfo);

    HANDLE processesSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
    if (processesSnapshot == INVALID_HANDLE_VALUE)
        return 0;

    Process32First(processesSnapshot, &processInfo);
    if (!processName.compare(processInfo.szExeFile))
    {
        CloseHandle(processesSnapshot);
        return processInfo.th32ProcessID;
    }

    while (Process32Next(processesSnapshot, &processInfo))
    {
        if (!processName.compare(processInfo.szExeFile))
        {
            CloseHandle(processesSnapshot);
            return processInfo.th32ProcessID;
        }
    }

    CloseHandle(processesSnapshot);
    return 0;
}

struct TimeSec
{ //t.tm_hour, t.tm_min, t.tm_sec

    time_t timestamp;

    void to_stream(std::ostream& str) const
    {
        struct tm t;
        _localtime64_s(&t, &timestamp);
        t.tm_year;
        t.tm_mday; // Проверка
        if (t.tm_mon + 1 > 12)
        {
            t.tm_mon = t.tm_mon - 12;
            t.tm_year++;
        }                                       // Даты
        auto date = ("%04i", t.tm_year + 1900); // Последнего
        auto date2 = ("%02i", t.tm_mon + 1);    // Изменения
        auto date3 = ("%02i", t.tm_mday);
        auto date4 = ("%02i:", t.tm_hour);
        auto date5 = ("%02i", t.tm_min);
        auto date6 = (": % 02i", t.tm_sec); // Файла
        str << date3 << "." << date2 << "." << date << " " << date4 << ":" << date5 << ":" << date6 ;
    }
};

std::ostream& operator<<(std::ostream& str, const TimeSec& time_marker)
{
    time_marker.to_stream(str);
    return str;
}
/**
 * Перемещаем закешированные обновления из папки с загрузками в папку назначения 
 * 
 * \param updates_dir
 * \param dest_dir
 */
void copy_cached_updates(const fs::path& updates_dir, const fs::path& dest_dir)
{

    BOOST_LOG_TRIVIAL(trace) << " [:::НАЧАЛ КОПИРОВАНИЕ И ЗАМЕНЯТЬ ФАЙЛЫ В ПАПКЕ ARMBASE:::] " ;

    using dir_iter = boost::filesystem::recursive_directory_iterator;
    bool isCanDeleteUpdateDir = false;
    for (auto&& item : boost::make_iterator_range(dir_iter{ updates_dir }, dir_iter{}))
    {
        auto ForBuffer = fs::relative(item, updates_dir);
        
        auto dest_di = dest_dir / ForBuffer;
        auto updates_di = updates_dir / ForBuffer;
        BOOST_LOG_TRIVIAL(trace) << "start " << item << " fro buffer " << ForBuffer<<" dest_di:"<< dest_di<<" updates_di:"<< updates_di;
        bool bAnyBrowserIsOpen = false;
        if (extension(dest_di) == ".exe") {
            std::wstring str_turned_to_wstr = dest_di.filename().wstring();
            if (FindProcessId(str_turned_to_wstr) && ForBuffer != "IRIupdater.exe") {
                bAnyBrowserIsOpen = true;
            }
        }
        if (!fs::exists(dest_di)) {
            if (fs::is_directory(item))
            {
                BOOST_LOG_TRIVIAL(trace) << "[Создал каталог в РАБОЧЕЙ папке armBase]: " << ForBuffer 
                    ;
                fs::create_directory(dest_di);
            }
            else
            {
                try
                {
                    BOOST_LOG_TRIVIAL(trace) << "[Перемещаю файл]:" << ForBuffer << "[Из]: " << item << "[В]: " << dest_di 
                        ;
                    fs::rename(item, dest_di);
                    BOOST_LOG_TRIVIAL(trace) << "Последнее изменение файла в ARMbase :" << TimeSec{ fs::last_write_time(item) } ;
                    BOOST_LOG_TRIVIAL(trace) << "Последнее изменение файла в dest directory :" << TimeSec{ fs::last_write_time(dest_di) } ;
                }
                catch (fs::filesystem_error& err)
                {
                    isCanDeleteUpdateDir = false;
                    BOOST_LOG_TRIVIAL(trace) << "Ошибка замены файла :" << err.what() << " path1:" << err.path1() << " path2:" << err.path2();
                    BOOST_LOG_TRIVIAL(trace) << "Ошибка замены файла" ;
                    cerr << err.what() << " path1:" << err.path1() << " path2:" << err.path2();
                    continue;
                }
            }
        }
        else
        {
            auto timestamp = fs::last_write_time(dest_di);
            auto timestamp2 = fs::last_write_time(item);
            if (timestamp2 > timestamp && !fs::is_directory(item) && bAnyBrowserIsOpen == 0)
            {
                if (updates_di.filename() == "IRIupdater.exe")
                { // Для обновление .exe !!!обновлятора!!!
                    try
                    {
                        BOOST_LOG_TRIVIAL(trace) << "[Перемещаю файл]:" << ForBuffer << "[Из]: " << item << "[В]: " << dest_di ;
                        /*BOOST_LOG_TRIVIAL(trace) << "[Скопировал файл]:" << ForBuffer ;
                        BOOST_LOG_TRIVIAL(trace) << "[Из]:::: " << item ;
                        BOOST_LOG_TRIVIAL(trace) << "[В]:::: " << dest_di ;
                        BOOST_LOG_TRIVIAL(trace) << "[ПРИМЕЧАНИЕ, ФАЙЛ IRIupdater скоировался как IRIupdater2.exe]" 
                            ;*/
                        fs::rename(item, dest_di.parent_path() / "IRIupdater2.exe");
                        BOOST_LOG_TRIVIAL(trace) << "Последнее изменение файла в ARMbase :" << TimeSec{ timestamp2 } ;
                        BOOST_LOG_TRIVIAL(trace) << "Последнее изменение файла в dest directory :" << TimeSec{ timestamp } ;
                    }
                    catch (fs::filesystem_error& err)
                    {
                        BOOST_LOG_TRIVIAL(trace) << "[Ошибка замены файла]:" << err.what() << " path1:" << err.path1() << " path2:" << err.path2();
                        BOOST_LOG_TRIVIAL(trace) << "Ошибка замены файла" ;
                        cerr << err.what() << " path1:" << err.path1() << " path2:" << err.path2();
                        continue;
                    }
                }
                else
                {
                    try {
                        BOOST_LOG_TRIVIAL(trace) << "[Перемещаю файл]:" << ForBuffer << "[Из]: " << item << "[В]: " << dest_di ;
                        BOOST_LOG_TRIVIAL(trace) << "[Получаю статус прав доступа dest_di]" ;
                        auto _dest_di_state = status(dest_di); //Получаю права доступа к старого файла
                        BOOST_LOG_TRIVIAL(trace) << "[замена файла в папке]" ;
                        fs::rename(item, dest_di);
                        BOOST_LOG_TRIVIAL(trace) << "[Возвращаю права доступа файлу dest_di]"<< _dest_di_state.permissions();
                        permissions(dest_di, _dest_di_state.permissions()); //Возвращаю  старые права доступа файла
                        BOOST_LOG_TRIVIAL(trace) << "Последнее изменение файла в ARMbase :" << TimeSec{ timestamp2 } << " dest directory : " << TimeSec{ timestamp };
                    } catch (fs::filesystem_error& err)
                    {
                        isCanDeleteUpdateDir = false;
                        BOOST_LOG_TRIVIAL(trace) << "[Ошибка замены файла]:" << err.what() << " path1:" << err.path1() << " path2:" << err.path2();
                        continue;
                    }
                }
            }
        }
    }
    if (isCanDeleteUpdateDir) {
        BOOST_LOG_TRIVIAL(trace) << "[:::НАЧАЛ УДАЛЯТЬ КАТАЛОГ BUFFER И ВСЕ ЕГО ФАЙЛЫ:::] : " << updates_dir.filename() 
            ;
        fs::remove_all(updates_dir);
        BOOST_LOG_TRIVIAL(trace) << "[:::ЗАКОНЧИЛ УДАЛЯТЬ КАТАЛОГ BUFFER И ВСЕ ЕГО ФАЙЛЫ:::] : " << updates_dir.filename() 
            ;
    }
    else {
        BOOST_LOG_TRIVIAL(trace) << "не могу удалить каталог для обновления. При копировании были ошибки";
    }
    BOOST_LOG_TRIVIAL(trace) << " [:::ЗАКОНЧИЛ КОПИРОВАНИЕ И ЗАМЕНЯТЬ ФАЙЛЫ В ПАПКЕ ARMBASE:::] " 
        ;
}

auto updateFile(const fs::directory_entry& item, const fs::path& download_di, const fs::path& ForTemp, const fs::path& updates_di)
{
    BOOST_LOG_TRIVIAL(trace) << "Перешел на обновление файла" ;
    SCheckResult res;
    res.calculate_file(item);
    if (fs::exists(download_di))
    {
        BOOST_LOG_TRIVIAL(trace) << "[Удалил битый файл]:" << ForTemp ;
        fs::remove(download_di);
        BOOST_LOG_TRIVIAL(trace) << "[Удалил]";
    }
    BOOST_LOG_TRIVIAL(trace) << "[Копирую файл]:" << ForTemp << "[Из]:" << item << "[В]: " << download_di ;
    boost::system::error_code er;
    fs::copy_file(item, download_di,boost::filesystem::copy_option::overwrite_if_exists, er);
    BOOST_LOG_TRIVIAL(trace) << "[Скопировал] код ошибки "<< er;

    if (fs::exists(download_di)) {
       // BOOST_LOG_TRIVIAL(trace) <<"boost c"
    }
    if (fs::exists(updates_di)) {
        BOOST_LOG_TRIVIAL(trace) << "[Удалил старый файл]:" << ForTemp ;
        fs::remove(updates_di);
        BOOST_LOG_TRIVIAL(trace) << "[Удалил]";
    }

    BOOST_LOG_TRIVIAL(trace) << "[перемещаю]: " << ForTemp << "[Из]: " << download_di << "[В]: " << updates_di;
    fs::rename(download_di, updates_di);
    BOOST_LOG_TRIVIAL(trace) << "[Переместил]";
    return res;
}

void updateDirectory(const fs::path& download_di, const fs::path& ForTemp, const fs::path& updates_di, const fs::path& dest_di)
{
    if (!fs::exists(download_di))
    {
        BOOST_LOG_TRIVIAL(trace) << "[Создал каталог в TEMP]: " << ForTemp 
            ;
        fs::create_directory(download_di);
    }
    if (!fs::exists(updates_di))
    {
        BOOST_LOG_TRIVIAL(trace) << "[Создал каталог в BUFFER]: " << ForTemp 
            ;
        fs::create_directory(updates_di);
    }
}

auto dowload_updates_from_src_to_update(const fs::path& src_dir, const fs::path& download_dir, const fs::path& updates_dir, const fs::path& dest_dir)
{
    SCheckResult res;
    BOOST_LOG_TRIVIAL(trace) << "_______________________________________________________" 
        ;
    BOOST_LOG_TRIVIAL(trace) << "[:::НАЧАЛ ПРОВЕРЯТЬ И КОПИРОВАТЬ ОБНОВЛЕНИЯ:::] " 
        ;
    
        using dir_iter = boost::filesystem::recursive_directory_iterator;
        auto src_dir_str = src_dir.string();
        auto src_dir_len = src_dir_str.size();

        // РЕКУРСИВЫНЫЙ ОБХОД ПАПОК И ФАЙЛОВ + ИХ СРАВНЕНИЯ НА ДАТУ СОЗДАНИЯ И ИХ СУЩЕСТВОВАНИЕ
        for (auto&& item : boost::make_iterator_range(dir_iter{ src_dir }, dir_iter{}))
        { 
            try
            {
            // Цик зависящий от количества файлов в сетевой папке программы
            fs::path now_path = item;
            fs::path ForTemp = now_path.string().substr(src_dir_len);
            // относительный путь от файла, котоырый идет по порядку в цикле и файла в сетевой
            // BOOST_LOG_TRIVIAL(trace)<<"ForTemp:" << ForTemp ;                        // проверка путей ВРЕМЕННАЯ
            auto download_di = download_dir / ForTemp; // сложение 2х частей path-а создавая конечный путь файла
            auto dest_di = dest_dir / ForTemp;         // тоже, что и вверху
            auto updates_di = updates_dir / ForTemp;   // тоже, что и вверху
            time_t timestamp_updates = 0;
            time_t timestamp_dest = 0;
            auto timestamp_item = fs::last_write_time(item); //-7200
            if (fs::exists(updates_di)) {
                timestamp_updates = fs::last_write_time(updates_di);
            }
            if (fs::exists(dest_di)) {
                timestamp_dest = fs::last_write_time(dest_di);
            }
#ifdef _DEBUG
            BOOST_LOG_TRIVIAL(trace) << "item:" << item << " time " << TimeSec{ timestamp_item } ;             // проверка путей ВРЕМЕННАЯ
            BOOST_LOG_TRIVIAL(trace) << "download_di:" << download_di ;                                      // проверка путей ВРЕМЕННАЯ
            BOOST_LOG_TRIVIAL(trace) << "updates_di:" << updates_di << " time " << TimeSec{ timestamp_dest } ; // проверка путей ВРЕМЕННАЯ
            BOOST_LOG_TRIVIAL(trace) << "dest_di:" << dest_di ;                                              // проверка путей ВРЕМЕННАЯ
#endif
            if (!fs::exists(dest_di))
            {
                if (fs::is_directory(item))
                {
                    updateDirectory(download_di, ForTemp, updates_di, dest_di);
                }
                else if (!fs::exists(updates_di) || timestamp_item > timestamp_updates)
                {
                    BOOST_LOG_TRIVIAL(trace) << "Начал обновлять файл " << updates_di << "time " << TimeSec{ timestamp_updates } << " файлом " << item << " time " << TimeSec{ timestamp_item } ;
                    res += updateFile(item, download_di, ForTemp, updates_di);
                    //BOOST_LOG_TRIVIAL(trace) << "Последнее изменение файла в сетевой :" << TimeSec{ timestamp_item } ;
                    //BOOST_LOG_TRIVIAL(trace) << "Последнее изменение файла в updates dir :" << TimeSec{ timestamp_updates };
                }
                else if (fs::exists(updates_di))
                {
                    res.calculate_file(item);
                }
            }
            else if (fs::exists(dest_di))
            {
                if (fs::is_directory(item))
                {
                    updateDirectory(download_di, ForTemp, updates_di, dest_di);
                }
                else if (timestamp_item > timestamp_dest && timestamp_item > timestamp_updates)
                {
                    BOOST_LOG_TRIVIAL(trace) << "Начал обновлять файл " << updates_di << " time " << TimeSec{ timestamp_dest } << " файлом  " << item << TimeSec{ timestamp_item } ;
                    res += updateFile(item, download_di, ForTemp, updates_di);
                    //BOOST_LOG_TRIVIAL(trace) << "Последнее изменение файла в сетевой :"<< TimeSec{ timestamp_item } <<endl;
                    //BOOST_LOG_TRIVIAL(trace) << "Последнее изменение файла в dest directory :"<< TimeSec{ timestamp_dest } ;
                }
                else if (fs::exists(updates_di))
                {
                    BOOST_LOG_TRIVIAL(trace) << "calculate file" << item;
                    res.calculate_file(item);
                }
            }
            else
            {
                continue;
            }
            }
            catch (fs::filesystem_error& err)
            {
                BOOST_LOG_TRIVIAL(trace) << "error in dowload_updates_from_src_to_update:" << err.what() << " path1:" << err.path1() << " path2:" << err.path2();
            }
        }
        if (res.is_have_update())
        {
            BOOST_LOG_TRIVIAL(trace) << "[:::НАЧАЛ УДАЛЯТЬ КАТАЛОГ BUFFER И ВСЕ ЕГО ФАЙЛЫ:::] : " << updates_dir ;
            fs::remove_all(updates_dir);
            BOOST_LOG_TRIVIAL(trace) << "[:::ЗАКОНЧИЛ УДАЛЯТЬ КАТАЛОГ BUFFER И ВСЕ ЕГО ФАЙЛЫ:::]" 
                ;
        }
        BOOST_LOG_TRIVIAL(trace) << "[:::ЗАКОНЧИЛ ПРОВЕРЯТЬ И КОПИРОВАТЬ ОБНОВЛЕНИЯ:::] " 
            ;
        BOOST_LOG_TRIVIAL(trace) << "_______________________________________________________" 
            ;
        
    
    return res;
}

vector<string> split(const string& str, const string& delim) //для сетевой папки
{
    vector<string> tokens;
    size_t prev = 0, pos = 0;
    do
    {
        pos = str.find(delim, prev);
        if (pos == string::npos)
            pos = str.length();
        string token = str.substr(prev, pos - prev);
        if (!token.empty())
            tokens.push_back(token);
        prev = pos + delim.length();
    } while (pos < str.length() && prev < str.length());
    return tokens;
}

void start_armbase(string armBase_path)
{
    string start = "start ";             //Запускает ARMbase
    armBase_path = start + armBase_path; //Если есть путь
    const char* cstr = armBase_path.c_str();
    system(cstr); //Вот тут запускает его
}

void exe_lock(boost::interprocess::file_lock &fileGuard) {
    boost::system_time t; 
    t = boost::get_system_time() +
    boost::posix_time::milliseconds(3000);
        if (fileGuard.timed_lock(t) == true)
        {
            // locked 
            BOOST_LOG_TRIVIAL(trace) << "<< file  locked >>" ;
        }
        else
        {
            BOOST_LOG_TRIVIAL(trace) << "<< file  no  locked >>" ;
            // not locked
        }
}

struct FileGuard {
    boost::interprocess::file_lock& lock_file;
    FileGuard(boost::interprocess::file_lock& _lock_file) :lock_file(_lock_file) {
        exe_lock(_lock_file);
    }

    ~FileGuard() {
        if (lock_file.try_lock() == true) {
            lock_file.unlock();
        }
    }
};

/**
 * Проверить что в каталоге лежать только exe файлы
 * 
 * \param path
 * \return 
 */
bool is_only_exe_in_dir(const fs::path& path) {
    using dir_iter = boost::filesystem::recursive_directory_iterator;
    for (auto&& item : boost::make_iterator_range(dir_iter{ path }, dir_iter{})) {
        if (fs::is_directory(item.path())) {
            continue;
        }
        if (fs::extension(item.path()) != ".exe") {
            return false;
        }
    }
    return true;
}

void init_boost_log(std::string log_path="") {
    auto core = boost::log::core::get();
    core->add_global_attribute("pid", boost::log::attributes::current_process_id());
    boost::log::add_common_attributes();
    boost::log::register_simple_formatter_factory<
        boost::log::trivial::severity_level, char>("update");
    /*core->set_filter
    (
        boost::log::trivial::severity >= boost::log::trivial::info
    );*/
    if (log_path != "") {
        //boost::log::add_file_log(log_path);
        
        boost::log::add_file_log(
            //boost::log::keywords::target = log_path,
            boost::log::keywords::auto_flush=true,
            //boost::log::keywords::max_files=10,
            //boost::log::keywords::max_size= 10 * 1024 * 1024,
            boost::log::keywords::file_name = log_path,
            boost::log::keywords::format = "[%pid%][%TimeStamp%]: %Message%",
            boost::log::keywords::open_mode = std::ios_base::app
        );
    }
    /*boost::log::add_console_log(
        std::cout, boost::log::keywords::format = "[%pid%]>> %Message%"
    );*/
}



auto main(int argc, char* argv[]) -> int {
    //FreeConsole();
   
    setlocale(LC_ALL, "Russian");

    std::string src_dir_path;/// Путь где должны лежать файлы
    std::string dest_dir;///Путь куда нужно положить файлы
    std::string download_dir;///Путь куда скачиваються обновления 
    std::string updates_dir;///Путь куда складываем обновления 
    std::string arm_base_path;///Путь к exe с armBase который нужно запустить 
    std::string logir; //Путь для записи лога
    std::string lock;///Путь к файлу который обеспецчивает блокировку 
    int i = 0;
    //std::string work_exe ="Обновление программы не может быть выполнено, закройте пожалуйста следующие приложения: \n";
    std::vector<std::string> src_dirs;///Может быть несколько каталогов для обновления 

    po::options_description desc("Allowed options"); //Создание проверки консоли на аргументы
    desc.add_options()("help", "show help message")
        //Qprocess нужен в armBase//
        ("src,S", po::value<std::string>(&src_dir_path)->default_value(NO_SIGNAL), "Source direction")       //Путь Сетевой папки
        ("dest,D", po::value<std::string>(&dest_dir)->default_value(NO_SIGNAL), "Destination direction")     //Путь Основной папки программы
        ("load,L", po::value<std::string>(&download_dir)->default_value(NO_SIGNAL), "Download direction")    //Путь Временной папки
        ("updates,U", po::value<std::string>(&updates_dir)->default_value(NO_SIGNAL), "Updater direction")   //Путь Буфера
        ("log,O", po::value<std::string>(&logir)->default_value(""), "Logir for programm")            //Путь для логирования
        ("lock,O", po::value<std::string>(&lock)->default_value(NO_SIGNAL), "Lock for programm")
        ("armBase,A", po::value<std::string>(&arm_base_path)->default_value(NO_SIGNAL), "ArmBase EXE path"); //Путь для повторного открытия ARMbase после обновления
    po::variables_map vm;
    po::store(parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    //arm_base_path = "C:\\projects\\output\\Win32\\ReleaseDbg\\ArmBase.lock";
    /*char buf[255];
    strcpy(buf, arm_base_path.c_str());*/
    try {
        init_boost_log(logir);
        struct tm timeinfo;
        time_t rawtime = time(0);
        localtime_s(&timeinfo, &rawtime);

        BOOST_LOG_TRIVIAL(trace)
            << "Старт: " << ctime(&rawtime);
        BOOST_LOG_TRIVIAL(trace) << "[ВЕРСИЯ " << __TIME__ << "]";
        BOOST_LOG_TRIVIAL(trace) << "[Папка из которой получаю обновления]:"
            << src_dir_path;
        BOOST_LOG_TRIVIAL(trace) << "[Папка ArmBase]:"
            << dest_dir;
        BOOST_LOG_TRIVIAL(trace) << "[Временная папка, куда 1 этапом копируются обновления]:"
            << download_dir;
        BOOST_LOG_TRIVIAL(trace) << "[Папка куда 2 этапом перемещаются обновления]:"
            << updates_dir;
        BOOST_LOG_TRIVIAL(trace) << "Путь до ArmBase " << arm_base_path << "\n";
        BOOST_LOG_TRIVIAL(trace) << "Путь до lock файла " << lock << "\n";
        std::unique_ptr<FileGuard> file_guard;
        if (lock != NO_SIGNAL) {
            ///Перехватываю блокировку Арма
            BOOST_LOG_TRIVIAL(trace) << " file_lock >>> " << lock << "status: ";
            boost::interprocess::file_lock f(lock.c_str());
            file_guard = std::make_unique<FileGuard>(f);
        }

        try
        {

            //Запись в файл/логирование
            //
            /*if (logir != NO_SIGNAL)
            { // если нет логера, то будет выписываться в консоль!!
                fs::path log = (logir);
                auto size = log.size() - log.filename().size();
                auto subtr = logir.substr(0, size);
                if (!fs::exists(subtr)) {
                    fs::create_directory(subtr);
                }
            }
            streambuf* BOOST_LOG_TRIVIAL(trace)buf = BOOST_LOG_TRIVIAL(trace).rdbuf();
            ofstream out(logir, std::ios::app);

            if (logir != NO_SIGNAL) {
                BOOST_LOG_TRIVIAL(trace).rdbuf(out.rdbuf());
            }*/

            if (src_dir_path != NO_SIGNAL)
            {
                src_dirs = split(src_dir_path, ",");
            }
            if (!fs::exists(download_dir) && download_dir != NO_SIGNAL)
            { // Если нет ВРЕМЕННОЙ ПАПКИ ПО ДАННОМУ ПУТИ, ТО СОЗДАЕТ
                BOOST_LOG_TRIVIAL(trace) << "СОЗДАЛ КАТАЛОГ TEMP::: "
                    ;
                fs::create_directory(download_dir);
            }
            /*if (!fs::exists(updates_dir) && download_dir != NO_SIGNAL)
            { // Если нет ПАПКИ ОБНОВЛЕНИЯ ПО ДАННОМУ ПУТИ, ТО СОЗДАЕТ
                BOOST_LOG_TRIVIAL(trace) << "СОЗДАЛ КАТАЛОГ BUFFER::: "  ;
                fs::create_directory(updates_dir);
            }*/
            BOOST_LOG_TRIVIAL(trace) << "[Проверка путей для закачивания обновления в TEMP - BUFFER]";
            BOOST_LOG_TRIVIAL(trace) << "src_dirs: " << src_dirs.empty()
                << "  fs::exists(download_dir): " << fs::exists(download_dir)
                << "  fs::exists(dest_dir): " << fs::exists(dest_dir);
            if (!src_dirs.empty() && fs::exists(download_dir) && fs::exists(dest_dir) && dest_dir != NO_SIGNAL && updates_dir != NO_SIGNAL)
            { //При запуске ARM base приходит
                // 4 path-а и происходит
                SCheckResult res;
                for (auto&& src_dir : src_dirs)
                {
                    if (!fs::exists(src_dir))
                    {
                        continue;
                    }
                    if (!fs::exists(updates_dir) && download_dir != NO_SIGNAL)
                    { // Если нет ПАПКИ ОБНОВЛЕНИЯ ПО ДАННОМУ ПУТИ, ТО СОЗДАЕТ
                        BOOST_LOG_TRIVIAL(trace) << "СОЗДАЛ КАТАЛОГ BUFFER::: "
                            ;
                        fs::create_directory(updates_dir);
                    }

                    res += dowload_updates_from_src_to_update(src_dir, download_dir, updates_dir, dest_dir); // запуск проверки обновления и скачивания.
                }
                if (res.is_only_exe_in_update())
                {
                    BOOST_LOG_TRIVIAL(trace) << "[:::В ПАПКЕ ТОЛЬКО .EXE -> ОБНОВЛЯЕМ БЕЗ НАЖАТИЯ НА КНОПКУ:::] "
                        ;
                    copy_cached_updates(updates_dir, dest_dir);
                }
            }
            BOOST_LOG_TRIVIAL(trace) << "[Проверка путей для замены и добавления файлов из BUFFER в ARMbase]";
            if (fs::exists(dest_dir) &&
                fs::exists(updates_dir) &&
                src_dir_path == NO_SIGNAL &&
                download_dir == NO_SIGNAL &&
                dest_dir != NO_SIGNAL &&
                updates_dir != NO_SIGNAL)
            {

                bool bAnyBrowserIsOpen = true;
                BOOST_LOG_TRIVIAL(trace) << "[Убиваю демона]";
                system("taskkill.exe /F /IM dbus-daemon.exe"); //dbus-daemon.exe
                if (FindProcessId(L"dbus-daemon.exe")) {
                    BOOST_LOG_TRIVIAL(trace) << "[Не убил демона]";
                    bAnyBrowserIsOpen = true;
                }
                else {
                    bAnyBrowserIsOpen = false;
                    BOOST_LOG_TRIVIAL(trace) << "[Убил демона]";
                }
                //Смотрим что у нас только exe файлы в директории для обновления
                if (is_only_exe_in_dir(updates_dir)) {
                    BOOST_LOG_TRIVIAL(trace) << "in update dir " << updates_dir << " only exe";
                    /// Пробуем скопировать, что скопировалось
                    copy_cached_updates(updates_dir, dest_dir);
                }

                //////// Начал проверять работающие файлы exe
                using dir_iter = boost::filesystem::recursive_directory_iterator;
                while (true)
                {
                    std::wstring work_exe = L"Обновление программы не может быть выполнено, закройте пожалуйста следующие приложения:\n";
                    for (auto&& item : boost::make_iterator_range(dir_iter{ dest_dir }, dir_iter{})) {
                        //BOOST_LOG_TRIVIAL(trace) << "Start process path "<< item<<"\n";
                        if (extension(item) == ".exe" && item.path().filename() != "IRIupdater.exe") {
                            std::wstring str_turned_to_wstr = item.path().filename().wstring();
                            if (FindProcessId(str_turned_to_wstr)) {
                                work_exe += item.path().filename().wstring() + L"\n";

                                i++;
                            }
                           
                        }
                    }
                    if (i > 0)
                    {

                        std::wstring stemp = std::wstring(work_exe.begin(), work_exe.end());
                        LPCWSTR sw = stemp.c_str();
                        if (MessageBox(NULL, sw, L"Обновление файлов ArmBase", MB_OKCANCEL) == IDCANCEL)
                        {
                            BOOST_LOG_TRIVIAL(trace) << "Dialog cancel - break";
                            src_dir_path = "";
                            dest_dir = "";
                            download_dir = "";
                            updates_dir = "";
                            i = 0;
                            break;
                        }
                        /*else if (MessageBox(NULL, sw, L"Обновление файлов ArmBase", MB_OKCANCEL) == IDOK)
                        {
                            i = 0;
                        }*/
                    }
                    else
                    {
                        break;
                    }
                }
                //////// Закончил проверять работающие exe файлы
                if (!updates_dir.empty() && !dest_dir.empty()) {
                    copy_cached_updates(updates_dir, dest_dir);
                } 
            }

            BOOST_LOG_TRIVIAL(trace) << "[:::КОНЕЦ ПРОГРАММЫ:::] ";
        }
        catch (fs::filesystem_error& err)
        {
            BOOST_LOG_TRIVIAL(trace) << "error " << err.what() << " path1:" << err.path1() << " path2:" << err.path2();
        }

        if (arm_base_path != NO_SIGNAL) {
            BOOST_LOG_TRIVIAL(trace) << "start restart arm:" << arm_base_path << "\n";
            start_armbase(arm_base_path);
        }
        else {
            BOOST_LOG_TRIVIAL(trace) << "Путь до ArmBase не указан";
        }

        BOOST_LOG_TRIVIAL(trace) << "all done";
        return 0;
    }
    catch (std::exception& ex) {
        BOOST_LOG_TRIVIAL(trace) << "have std tuntime error"<< ex.what();
        return 0;
    }
    catch (...) {
        BOOST_LOG_TRIVIAL(trace) << "have uncnown error";
        return -1;
    }
}
